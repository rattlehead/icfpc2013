package com.icfpc2013.sexp.parser;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Scanner;

import org.junit.BeforeClass;
import org.junit.Test;

import com.icfpc.sexp.element.ExpElement;
import com.icfpc.sexp.parser.ExpSubtree;
import com.icfpc.sexp.parser.SExpParser;

public class SExpParser1Test extends SExpParserTestAbstract{

	@SuppressWarnings("resource")
	@BeforeClass
	public static void beforeClass() throws FileNotFoundException {		
		Scanner sc = new Scanner( new File("src/test/resources/test1"));
		readFile(sc);
	}

}
