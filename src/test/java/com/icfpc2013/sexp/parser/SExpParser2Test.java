package com.icfpc2013.sexp.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.BeforeClass;

public class SExpParser2Test  extends SExpParserTestAbstract{

	@SuppressWarnings("resource")
	@BeforeClass
	public static void beforeClass() throws FileNotFoundException {		
		Scanner sc = new Scanner( new File("src/test/resources/test2"));
		readFile(sc);
	}

}