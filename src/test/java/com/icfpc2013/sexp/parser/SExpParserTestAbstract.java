package com.icfpc2013.sexp.parser;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Scanner;

import org.junit.Test;

import com.icfpc.converter.HexToDecimalConverter;
import com.icfpc.sexp.element.ExpElement;
import com.icfpc.sexp.parser.ExpSubtree;
import com.icfpc.sexp.parser.SExpParser;

public class SExpParserTestAbstract {	
	protected static String testLine;
	protected static Long[] in = new Long[1000];
	protected static Long[] results = new Long[1000];
	protected static int testSize = 0;

	public static void readFile(Scanner sc) {
		testLine = sc.nextLine();
		testSize = 0;
		while (sc.hasNext()) {
			String inS = sc.next();
			String outS = sc.next();
			in[testSize] = HexToDecimalConverter.convertToDec(inS);
			results[testSize] = HexToDecimalConverter.convertToDec(outS);
			++testSize;
		}
	}
	
	@Test
	public void testFile() {
		for (int i = 0; i < testSize; i++) {
			SExpParser parser = new SExpParser();
			ExpElement tree = parser.parse(testLine);
			System.out.println(tree);
			String argName = "x";
			if (tree instanceof ExpSubtree) {
				argName = ((ExpSubtree) tree).processProgramArg();
			}
			HashMap<String, Long> valueMap = new HashMap<String, Long>();
			valueMap.put(argName, in[i]);
			tree.setValueMap(valueMap);
			Long result = tree.eval(new long[] { in[i] });
			System.out.println(result);
			assertEquals(results[i], result);
		}
	}
}
