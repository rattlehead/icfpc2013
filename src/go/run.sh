#!/bin/sh

export GOPATH=$(dirname $(realpath $0))

GOMAXPROCS=8 go run ${GOPATH}/src/main.go $@ 
