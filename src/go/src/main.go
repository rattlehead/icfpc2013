package main

import "os"
import "errors"
import "time"
import "bufio"
import "strconv"
import "strings"
import "fmt"
import . "icfpc13"

func wait_for_zero(input chan int) {
    c := 0
    for diff := range input {
        c += diff
        if c == 0 {
            close(input)
            return
        }
    }
}

type Result struct {
    solution Lambda
    result string
}

func matches(el Lambda, points [][2]uint64) bool {
    ns := map[string]uint64{}
    for _, point := range points {
        args := []uint64{point[0]}

        if Apply(el, args, ns) != point[1] {
            //if (el.String() == "(lambda (x) (xor 1 x))") {
            //    fmt.Println(point[0], Apply(el, args, ns), point[1])
            //}
            return false
        }
    }
    return true
}

func main() {
    data_points := make([][2]uint64, 0)

    in := bufio.NewReader(os.Stdin)
    size_str, err := in.ReadString('\n')
    if err != nil {
        errors.New("Can't read size")
    }
    max_size, err := strconv.Atoi(strings.TrimSpace(size_str))
    if err != nil {
        errors.New("Failed parsing size")
    }
    operators_line, err := in.ReadString('\n')
    if err != nil {
        errors.New("Can't read operators")
    }
    operators := strings.Split(strings.TrimSpace(operators_line), " ")
    if len(operators) == 1 && operators[0] == "" {
        operators = []string{"not", "shl1", "shr1", "shr4", "shr16", "and", "or", "xor", "plus", "if0", "lambda"}
    }

    var i int
    for i = 0;; i++ {
        line, err := in.ReadString('\n')
        if err != nil {
            errors.New("Failed reading data line")
        }
        line = strings.TrimSpace(line)
        if line == "" {
            break
        }
        number_strs := strings.Split(line, " ")
        var point [2]uint64
        point[0], _ = strconv.ParseUint(number_strs[0], 0, 64)
        point[1], _ = strconv.ParseUint(number_strs[1], 0, 64)
        data_points = append(data_points, point)
    }
    data_points = data_points[:i]
    //fmt.Fprintln(os.Stderr, data_points)
    //fmt.Fprintln(is.Stderr, operators)

    solutions := make(chan Lambda, 1000)
    operators_map := make(map[string]bool)
    for _, op := range operators {
        operators_map[op] = true
    }
    go GeneratePrograms(max_size, operators_map, solutions)
    results := make(chan Result, 3000)
    c := 0

    matchers_done := make(chan bool)
    defer func() {
        <-matchers_done
    }()
    matchers_running := make(chan int, 5)
    go func() {
        num := 0
        finalize := false
        for diff := range matchers_running {
            if diff == 100 {
                finalize = true
                //fmt.Println("got 100")
            } else {
                num += diff
                //fmt.Println(num-diff,diff,num)
            }
            if finalize && num == 0 {
                matchers_done <- true
            }
        }
    }()

    finalized := false
    var time_taken time.Duration = 0
    last_time := time.Now()
    go func() {
        for {

            result := <-results
            // Stop if found solution
            if (result.result == "win") {
                //fmt.Fprintf(os.Stderr, "Win")
                fmt.Println(result.solution)
                matchers_running <- 100
                finalized = true
                return
            }
        }
    }()

    timer := time.NewTimer(time.Second)
    death_timer := time.NewTimer(5 * time.Minute)

    for {
        //fmt.Println("iter")
        if finalized { break }
        select {
            case solution, ok := <-solutions:
                time_taken += time.Since(last_time)
                if !ok {
                    if finalized { continue }
                    //fmt.Fprintln(os.Stderr, "Tried all programs")
                    matchers_running <- 100
                    finalized = true
                    continue
                } else {
                    //-----------
                    //fmt.Fprintln(os.Stderr, solution)
                    c += 1
                    if c == 60000000 {
                        matchers_running <- 100
                        finalized = true
                        continue
                    }
                    if c % 100000 == 0 {
                        fmt.Fprintln(os.Stderr, c, time_taken)
                        time_taken = 0
                    }
                    matchers_running <- 1
                    go func(e Lambda) {
                        var result string
                        if matches(e, data_points) {
                            result = "win"
                        } else {
                            result = "fail"
                        }

                        results <- Result{e, result}
                        matchers_running <- -1
                    }(solution)
                    last_time = time.Now()
                }
                timer.Reset(time.Second)
            case <-timer.C:
                timer.Reset(time.Second)
            case <-death_timer.C:
                finalized = true
                continue

        }
        if finalized { break }
    }
    //fmt.Fprintln(os.Stderr, "Fell off main")
}
