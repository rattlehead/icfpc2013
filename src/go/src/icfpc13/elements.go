package icfpc13

import "fmt"
import "strings"

type Elem interface {
    fmt.Stringer
    Size() int
    Eval(map[string]uint64) uint64
}

// ---- Atom ----

type Atom struct {
    Value string
}

func (el Atom) String() string {
    return el.Value
}
func (el Atom) Size() int {
    return 1
}
func (el Atom) Eval(namespace map[string]uint64) uint64 {
    if el.Value == "0" { return 0 }
    if el.Value == "1" { return 1 }
    value, ok := namespace[el.Value]
    if !ok {
        panic("Unresolved atom " + el.Value)
    }
    return value
}

// ---- Op1  ----

type Op1 struct {
    Name string
    Operand Elem
}
func (el Op1) String() string {
    return "(" + el.Name + " " + el.Operand.String() + ")"
}
func (el Op1) Size() int {
    return 1 + el.Operand.Size()
}

func (el Op1) Eval(namespace map[string]uint64) uint64 {
    if el.Name == "not" { return ^el.Operand.Eval(namespace) }
    if el.Name == "shl1" { return el.Operand.Eval(namespace)<<1 }
    if el.Name == "shr1" { return el.Operand.Eval(namespace)>>1 }
    if el.Name == "shr4" { return el.Operand.Eval(namespace)>>4 }
    if el.Name == "shr16" { return el.Operand.Eval(namespace)>>16 }
    panic("Should not have got here.")
    return 0;
}

var op1s = []string{"not", "shl1", "shr1", "shr4", "shr16"}

// ---- Op2  ----

type Op2 struct {
    Name string
    Operand1 Elem
    Operand2 Elem
}
func (el Op2) String() string {
    return fmt.Sprintf("(%s %s %s)", el.Name, el.Operand1.String(), el.Operand2.String())
}
func (el Op2) Size() int {
    return 1 + el.Operand1.Size() + el.Operand2.Size()
}
func (el Op2) Eval(namespace map[string]uint64) uint64 {
    if (el.Name == "and") { return el.Operand1.Eval(namespace) & el.Operand2.Eval(namespace) }
    if (el.Name == "or") { return el.Operand1.Eval(namespace) | el.Operand2.Eval(namespace) }
    if (el.Name == "xor") { return el.Operand1.Eval(namespace) ^ el.Operand2.Eval(namespace) }
    if (el.Name == "plus") { return el.Operand1.Eval(namespace) + el.Operand2.Eval(namespace) }
    panic("Should not have got here.")
    return 0;
}
var op2s = []string{"and", "or", "xor", "plus"}

// ---- If0  ----

type If0 struct {
    Cond Elem
    IfTrue Elem
    IfFalse Elem
}
func (el If0) String() string {
    return fmt.Sprintf("(if0 %s %s %s)", el.Cond.String(), el.IfTrue.String(), el.IfFalse.String())
}
func (el If0) Size() int {
    return 1 + el.Cond.Size() + el.IfTrue.Size() + el.IfFalse.Size()
}
func (el If0) Eval(namespace map[string]uint64) uint64 {
    if (el.Cond.Eval(namespace) == 0) {
        return el.IfTrue.Eval(namespace)
    } else {
        return el.IfFalse.Eval(namespace)
    }
}

// ---- Lambda  ----

type Lambda struct {
    Args [2]string
    Expr Elem
}
func (el Lambda) String() string {
    return fmt.Sprintf("(lambda (%s) %s)", strings.Join(el.Args[:], " "), el.Expr.String())
}
func (el Lambda) Size() int {
    return 1 + el.Expr.Size();
}
func (el Lambda) Eval(namespace map[string]uint64) uint64 {
    panic("Lambda being evaled")
    return 0
}
func Apply(el Lambda, values []uint64, namespace map[string]uint64) uint64 {
    new_ns := make(map[string]uint64)

    for k, v := range namespace {
        new_ns[k] = v
    }
    for i, arg_name := range el.Args {
        if arg_name != "" {
            new_ns[arg_name] = values[i]
        }
    }
    return el.Expr.Eval(new_ns)
}


// ---- Fold  ----

type Fold struct {
    Arg Elem
    Acc Elem
    Fun Lambda
}
func (el Fold) String() string {
    return fmt.Sprintf("(fold %s %s %s)", el.Arg.String(), el.Acc.String(), el.Fun.String())
}
func (el Fold) Size() int {
    return 1 + el.Arg.Size() + el.Acc.Size() + el.Fun.Size()
}
func (el Fold) Eval(namespace map[string]uint64) uint64 {
    arg := el.Arg.Eval(namespace)
    acc := el.Acc.Eval(namespace)
    for i := 0; i < 8; i++ {
        acc = Apply(el.Fun, []uint64{arg & 255, acc}, namespace)
        arg = arg >> 8
    }
    return acc
}
