package icfpc13

import "fmt"

func wait_for_zero(input chan int) {
    c := 0
    for diff := range input {
        c += diff
        if c == 0 {
            close(input)
            return
        }
    }
}

func generate_all_expression(exact_size int,
                             free_vars []string,
                             operators map[string]bool,
                             output chan Elem,
                             toplevel bool,
                             had_fold bool) {
    writers_change := make(chan int, 300)
    total_writers := 0
    defer func() {
        if total_writers > 0 {
            wait_for_zero(writers_change)
        }
        close(output)
    }()

    _, fold := operators["fold"]
    _, tfold := operators["tfold"]
    // As fold with trivial lambda reduces to not fold, don't exit here.
    //if toplevel && (tfold) && (exact_size < 5) && !had_fold { return }

    if fold && !had_fold {
        for op1_size := 1; op1_size <= (exact_size - 1) - 3; op1_size++ {
            for op2_size := 1; op2_size <= (exact_size - 1) - 2 - op1_size; op2_size++ {
                op3_size := (exact_size - 1) - op1_size - op2_size

                children1 := make(chan Elem)
                go generate_all_expression(op1_size, free_vars, operators, children1, false, true)
                writers_change <- 1
                total_writers += 1
                go func(op1_size, op2_size int) {
                    for i := range children1 {
                        children2 := make(chan Elem)
                        go generate_all_expression(op2_size, free_vars, operators, children2, false, true)
                        writers_change <- 1
                        go func(i Elem) {
                            for j := range children2 {
                                children3 := make(chan Lambda)
                                go generate_all_lambdas(op3_size, free_vars, operators, children3, true)
                                writers_change <- 1
                                go func(j Elem) {
                                    for k := range children3 {
                                        e := k.Expr
                                        if (e == Atom{"0"} || e == Atom{"1"} ||
                                            e == Atom{"x"} || e == Atom{"y"} ||
                                            e == Atom{"z"}) { continue }
                                        output <- Fold{Arg: i, Acc: j, Fun: k}
                                    }
                                    writers_change <- -1
                                }(j)
                            }
                            writers_change <- -1
                        }(i)
                    }
                    writers_change <- -1
                }(op1_size, op2_size)
            }
        }
    }

    if toplevel && tfold && exact_size > 4 {

            op1_size := 1
            op2_size := 1

            op3_size := exact_size - 1 - op1_size - op2_size

            i := Atom{"x"}
            j := Atom{"0"}

            childrens := make([]chan Lambda, op3_size-1)
            for child_size := 2; child_size <= op3_size; child_size ++ {
                childrens[child_size-2] = make(chan Lambda)
                go generate_all_lambdas(child_size, free_vars, operators,
                        childrens[child_size-2], true)
            }

            for _, children := range childrens {
                for k := range children {
                    e := k.Expr
                    if (e == Atom{"0"} || e == Atom{"1"} ||
                        e == Atom{"x"} || e == Atom{"y"} ||
                        e == Atom{"z"}) { continue }
                    output <- Fold{Arg: i, Acc: j, Fun: k}
                }
            }
        return
    }

    if exact_size <= 0 {
        panic("exact_size < 0")
    }
    if exact_size == 1 {
        output <- Atom{"0"}
        output <- Atom{"1"}
        for _, v := range free_vars {
            output <- Atom{v}
        }
        return
    }

    for _, op := range(op1s) {

        _, use := operators[op]
        if !use { continue }
        children := make(chan Elem)
        go generate_all_expression(exact_size - 1, free_vars, operators, children, false, had_fold)
        for i := range children {
            if ((op == "shr1" || op == "shr4" || op == "shr16") &&
                (i == Atom{"0"} || i == Atom{"1"})) { continue }
            if (op == "shl1" && i == Atom{"0"}) { continue }
            // Avoid (not (not
            if op == "not" {
                inner, ok := i.(Op1)
                if ok && inner.Name == "not" {
                    continue
                }
            }
            output <- Op1{Name: op, Operand: i}
        }
    }

    if exact_size == 2 {
        return
    }


    for _, op := range(op2s) {
        _, use := operators[op]
        if !use { continue }

        var op2_size int
        for op1_size := 1; op1_size <= exact_size - 2; op1_size++ {
            op2_size = exact_size - op1_size - 1
            children1 := make(chan Elem)
            go generate_all_expression(op1_size, free_vars, operators, children1, false, had_fold)
            for i := range children1 {
                // op2 0 * is boring.
                // op2 1 * is proceed as op2 * 1.
                if (i == Atom{"0"}) { continue }
                children2 := make(chan Elem)
                go generate_all_expression(op2_size, free_vars, operators, children2, false, had_fold)

                for j := range children2 {
                    // All (op * 1) processed as (op 1 *)
                    if (j == Atom{"1"}) { continue }
                    // All (op * 0) are boring
                    if (j == Atom{"0"}) { continue }

                    if ((op != "plus") && (i == j)) { continue }
                    output <- Op2{op, i, j}
                }
            }
        }
    }

    if exact_size == 3 {
        return
    }

    _, use := operators["if0"]
    if use {
        for op1_size := 1; op1_size <= (exact_size - 1) - 2; op1_size++ {
            for op2_size := 1; op2_size <= (exact_size - 1) - op1_size - 1; op2_size++ {
                op3_size := (exact_size - 1) - op1_size - op2_size

                children1 := make(chan Elem)
                go generate_all_expression(op1_size, free_vars, operators, children1, false, had_fold)
                for i := range children1 {
                    if (i == Atom{"0"} || i == Atom{"1"}) { continue }
                    children2 := make(chan Elem)
                    go generate_all_expression(op2_size, free_vars, operators, children2, false, had_fold)
                    for j := range children2 {
                        children3 := make(chan Elem)
                        go generate_all_expression(op3_size, free_vars, operators, children3, false, had_fold)
                        for k := range children3 {
                            if ((j == Atom{"0"} && i == k) ||
                                (i == j && k == Atom{"0"}) ||
                                (j == k)) { continue }
                            output <- If0{Cond: i, IfTrue: j, IfFalse: k}
                        }
                    }
                }
            }
        }
    }
}

func stringInSlice(a string, list []string) bool {
    for _, b := range list {
        if b == a {
            return true
        }
    }
    return false
}

func generate_all_lambdas(exact_size int, free_vars []string,
                          operators map[string]bool, output chan Lambda,
                          had_fold bool) {
    if exact_size < 2 {
        panic("generate lambda exact_size < 2")
    }
    var new_var_names [2]string = [2]string{"y", "z"}
    more_vars := append(free_vars, "y", "z")//make([]string, len(free_vars) + 2)
    //copy(more_vars, free_vars)
    //copy(more_vars[len(free_vars):], new_var_names[:])

    children := make(chan Elem)
    go generate_all_expression(exact_size - 1, more_vars, operators, children, false, had_fold)

    //go func() {
    for i := range children {
        output <- Lambda{new_var_names, i}
    }

    close(output)
    //}()
}

func GeneratePrograms(max_size int, operators map[string]bool, output chan Lambda) {
    // Just to keep fmt import.
    if false { fmt.Println("") }
    bodies := make([]chan Elem, max_size-1)
    for i := 1; i <= max_size - 1; i++ {
        bodies[i-1] = make(chan Elem)
        go generate_all_expression(i, []string{"x"}, operators, bodies[i-1], true, false)
    }

    for _, body := range bodies {
        for el := range body {
            var args [2]string = [2]string{"x", ""}
            output <- Lambda{args, el}
        }
    }
    close(output)
}
