package com.icfpc.python;

import org.python.core.PyInstance;
import org.python.core.PyObject;
import org.python.core.PyString;
import org.python.util.PythonInterpreter;

public class FindFunctionPyInterpreter {
	private PythonInterpreter interpreter = null;

	public void init() {
		PythonInterpreter.initialize(System.getProperties(),
				System.getProperties(), new String[0]);
		this.interpreter = new PythonInterpreter();
		//this.interpreter.exec("from __future__ import generators");
	}
	
	public void exec(String exec) {
		this.interpreter.exec(exec);
	}

	public void execfile(final String fileName) {
		this.interpreter.execfile(fileName);
	}

	public PyInstance createClass(final String className, final String opts) {
		return (PyInstance) this.interpreter.eval(className + "(" + opts + ")");
	}
	
	public void runFunc(String funcName) {
		PyObject someFunc = this.interpreter.get(funcName);
		PyObject result = someFunc.__call__();
		String realResult = (String) result.__tojava__(String.class);
		System.out.println(realResult);
	}
	
	public static void main(String[] args) {
		FindFunctionPyInterpreter interpeter = new FindFunctionPyInterpreter();
		interpeter.init();
		interpeter.execfile("src/python/program.py");
		//interpeter.runFunc("main");
	}
}
