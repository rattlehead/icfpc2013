package com.icfpc.comparator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.icfpc.converter.HexToDecimalConverter;
import com.icfpc.sexp.ProblemCalculator;

public class ProgramHashComparator {
	public static void outProgramByHash(String hash, String fileName) throws FileNotFoundException {
		Scanner sc = new Scanner(new File(fileName));
		while(sc.hasNext()) {
			String line = sc.nextLine();
			String[] split = line.split("\\|");
			String func = split[0];
			String funcHash = split[1];
			if (hash.equals(funcHash)) {
				System.out.println(func);
			}
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		Scanner sc = new Scanner(System.in);
		String outsHex = sc.nextLine();
		String outsDec = convertHexListToDec(outsHex);
		outProgramByHash("" + ProblemCalculator.getCRC64Hash(outsDec), args.length == 0 ? "c:/out_7_hash.txt" : args[0]);
		//System.out.print(HexToDecimalConverter.convertToDec("0xFFFFFFFFFFFFFFFF"));		
	}
	
	private static String convertHexListToDec(String outsHex) {
		String outDecs = "";
		for(String out : outsHex.split(" ")) {
			if(out.length()>0) {
				long value = HexToDecimalConverter.convertToDec(out);
				outDecs += " " + value;
			}			
		}
		return outDecs;
	}
}
