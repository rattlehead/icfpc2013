package com.icfpc.sexp.element;

import java.util.Map;

public abstract class AbstractExpElement implements ExpElement{

	public abstract int elementsAmount();

	public abstract long evalIfGoodX(long[] x);
	
	public long eval(long[] x) {
		if((x == null && elementsAmount() != 0) || x.length < elementsAmount()) {
			throw new RuntimeException("Function is not supported such amount of elements");
		} else {
			return evalIfGoodX(x);
		}
	}

	public void setValueMap(Map<String, Long> valueMap) {
	}
}
