package com.icfpc.sexp.element;

import java.util.Map;

public class AtomExpElement<T> extends AbstractExpElement {

    private T element;

    public AtomExpElement(T element) {
        setElement(element);
    }

    public T getElement() {
        return element;
    }

    public void setElement(T element) {
        this.element = element;
    }

    public String toString() {
        return element.toString();
    }

	@Override
	public int elementsAmount() {
		return 1;
	}

	@Override
	public long evalIfGoodX(long[] x) {
		if(element instanceof Long) {
			return (Long) element;
		} else {
			// TODO
			return x[0];
		}
	}

	public void setValueMap(Map<String, Long> valueMap) {
	}
}
