package com.icfpc.sexp.element;

import java.util.Map;

public class VariableAtomExpElement extends AtomExpElement<String>{
	private Map<String, Long> valueMap;
	public VariableAtomExpElement(String element) {
		super(element);
	}

	public void setValueMap(Map<String, Long> valueMap) {
		this.valueMap = valueMap;
	}
	
	@Override
	public long evalIfGoodX(long[] x) {
		Long value = valueMap.get(getElement());
		if(value == null) {
			throw new RuntimeException(getElement() + " value has not specified in the map yet");
		} else {
			return value;
		}
	}
	
	
}
