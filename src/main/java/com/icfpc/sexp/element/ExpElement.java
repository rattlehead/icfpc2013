package com.icfpc.sexp.element;

import java.util.Map;

public interface ExpElement {
	long eval(long[] x);
	void setValueMap(Map<String, Long> valueMap);
}
