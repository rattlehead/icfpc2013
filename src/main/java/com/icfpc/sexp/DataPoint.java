package com.icfpc.sexp;

public class DataPoint {

    private long input;

    private long result;

    public DataPoint(long input, long result) {
        setInput(input);
        setResult(result);
    }

    public long getInput() {
        return input;
    }

    public void setInput(long input) {
        this.input = input;
    }

    public long getResult() {
        return result;
    }

    public void setResult(long result) {
        this.result = result;
    }
}
