package com.icfpc.sexp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.icfpc.sexp.element.AtomExpElement;
import com.icfpc.sexp.element.ExpElement;
import com.icfpc.sexp.parser.ExpSubtree;
import com.icfpc.sexp.element.VariableAtomExpElement;

import com.icfpc.sexp.parser.operations.*;

public class ProgramGenerator {

    // map: maxSize -> all solutions
    private Map<Integer, List<ExpElement>> solutionMap;

    public ProgramGenerator() {
        solutionMap = new HashMap<Integer, List<ExpElement>>();
    }

    public ExpElement findFunction(int maxSize, List<DataPoint> dataPoints) throws Exception {

        List<VariableAtomExpElement> freeVars = Arrays.asList(new VariableAtomExpElement("x"));
        generateFunctions(maxSize, freeVars);

        return null;
    }

    private List<ExpElement> generateFunctions(int size, List<VariableAtomExpElement> freeVars) throws Exception {

        assert size >= 1;

        List<ExpElement> solutions = solutionMap.get(size);
        if (solutions != null) {
            return solutions;
        } else {
            solutions = new ArrayList<ExpElement>();
            System.out.println("generating for size " + size);
            if (size <= 7) {
                solutionMap.put(size, solutions);
            }
        }

        if (size == 1) {
            solutions.add(new AtomExpElement<Long>(0L));
            solutions.add(new AtomExpElement<Long>(1L));
            for (VariableAtomExpElement var : freeVars) {
                solutions.add(var);
            }

            return solutions;
        }

        for (Class op1 : Arrays.asList(NotExpOperation.class, Shl1ExpOperation.class, Shr1ExpOperation.class,
                Shr4ExpOperation.class, Shr16ExpOperation.class)) {
            for (ExpElement e : generateFunctions(size - 1, freeVars)) {
                ExpSubtree subtree = new ExpSubtree();
                subtree.addChild((ExpElement) op1.newInstance());
                subtree.addChild(e);
                solutions.add(subtree);
            }
        }

        if (size == 2) {
            return solutions;
        }

        for (Class op2 : Arrays.asList(AndExpOperation.class, OrExpOperation.class, XorExpOperation.class,
                PlusExpOperation.class)) {
            for (int arg1Len = 1; arg1Len < size - 1; arg1Len++) {
                int arg2Len = size - 1 - arg1Len;
                for (ExpElement arg1 : generateFunctions(arg1Len, freeVars)) {
                    for (ExpElement arg2 : generateFunctions(arg2Len, freeVars)) {
                        ExpSubtree subtree = new ExpSubtree();
                        subtree.addChild((ExpElement) op2.newInstance());
                        subtree.addChild(arg1);
                        subtree.addChild(arg2);
                        solutions.add(subtree);
                    }

                }
            }
        }

        if (size == 3) {
            return solutions;
        }

        for (int arg1Len = 1; arg1Len < size - 2; arg1Len++) {
            for (int arg2Len = 1; arg2Len < size - 1 - arg1Len; arg2Len++) {
                int arg3Len = size - 1 - arg1Len - arg2Len;
                for (ExpElement arg1 : generateFunctions(arg1Len, freeVars)) {
                    for (ExpElement arg2 : generateFunctions(arg2Len, freeVars)) {
                        for (ExpElement arg3 : generateFunctions(arg3Len, freeVars)) {
                            ExpSubtree subtree = new ExpSubtree();
                            subtree.addChild(new IfExpOperation());
                            subtree.addChild(arg1);
                            subtree.addChild(arg2);
                            subtree.addChild(arg3);
                            solutions.add(subtree);
                        }
                    }
                }
            }
        }
        
        if (size == 4) {
            return solutions;
        }

        return solutions;
    }

    public static void main(String[] argv) throws Exception {
        int n = 7;
        List<VariableAtomExpElement> freeVars = Arrays.asList(new VariableAtomExpElement("x"));

        ProgramGenerator pg = new ProgramGenerator();

        int i = 0;
        
        for (ExpElement e : pg.generateFunctions(n, freeVars)) {
//            System.out.println(e);
            i++;
            if (i % 1000 == 0) {
                System.out.println("Generated: " + i);
            }
        }
        
        System.out.println("Done");
    }
}
