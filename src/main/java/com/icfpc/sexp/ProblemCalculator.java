package com.icfpc.sexp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

import com.icfpc.converter.HexToDecimalConverter;
import com.icfpc.sexp.element.ExpElement;
import com.icfpc.sexp.parser.ExpSubtree;
import com.icfpc.sexp.parser.SExpParser;

public class ProblemCalculator {
	private static final long []CRC_TABLE;
	private static final long POLY64REV = 0xd800000000000000L;
	
	static {
	    CRC_TABLE = new long[256];

	    for (int i = 0; i < 256; i++) {
	      long v = i;

	      for (int j = 0; j < 8; j++) {
	        boolean flag = (v & 1) != 0;

	        long newV = v >>> 1;

	        if ((v & 0x100000000L) != 0)
	          newV |= 0x100000000L;

	        if ((v & 1) != 0)
	          newV ^= POLY64REV;

	        v = newV;
	      }

	      CRC_TABLE[i] = v;
	    }
	  }
	

	public void calculate(String fileProblems, String fileIn, String fileOut) throws FileNotFoundException, UnsupportedEncodingException {
		Long[] inputs = readInputs(fileIn);
		Scanner scProblems = new Scanner(new File(fileProblems));
		PrintWriter outWriter = new PrintWriter(fileOut, "UTF-8");
		int amount = 0;
		while(scProblems.hasNext()) {
			String expProblem = scProblems.nextLine();
			if(amount % 1000 == 0) {
				System.out.println(amount + ": " + expProblem);
			}
			++amount;
			SExpParser parser = new SExpParser();
			ExpElement tree = parser.parse(expProblem);
			String argName = ((ExpSubtree) tree).processProgramArg();
			HashMap<String, Long> valueMap = new HashMap<String, Long>();
			tree.setValueMap(valueMap);
			
			String outs = "";
			
			for(int i = 0; i < inputs.length; i++) {
				Long in = inputs[i];
				valueMap.clear();
				valueMap.put(argName, in);
				outs += " " + tree.eval(new long[] {in});
			}
			outWriter.println(expProblem + "|" + getCRC64Hash(outs));
		}
		outWriter.close();
		scProblems.close();
	}
	
	public static long getCRC64Hash(String value)
	  {
	    long crc = 40002877L;
	    int  len = value.length();

	    for (int i = 0; i < len; i++) {
	      char ch = value.charAt(i);

	      if (ch > 0xff)
	        crc = next(crc, (ch >> 8));

	      crc = next(crc, ch);
	    }

	    return crc;
	  }

	  public static long next(long crc, int ch)
	  {
	    return (crc >>> 8) ^ CRC_TABLE[((int) crc ^ ch) & 0xff];
	  }
	
	private Long[] readInputs(String fileIn) throws FileNotFoundException {
		Scanner sc = new Scanner(new File(fileIn));
		String inLine = sc.nextLine();
		String[] inLineArray = inLine.split(" ");
		Long[] result = new Long[inLineArray.length];
		for(int i = 0; i < inLineArray.length; i++) {
			result[i] = HexToDecimalConverter.convertToDec(inLineArray[i]);
		}
		sc.close();
		return result;
	}
	
	public Long[] generateRandomLongs(int amount) {
		Long[] result = new Long[amount];
		Random rng = new Random();
		for(int i = 0; i < amount; i++) {
			result[i] = nextLong(rng);
		}
		return result;
	}
	
	long nextLong(Random rng) {
		   long bits, val;
		   do {
		      bits = (rng.nextLong() << 1) >>> 1;
		      val = bits;
		   } while (bits-val+(Long.MAX_VALUE-1) < 0L);
		   return val;
		}

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		ProblemCalculator calculator = new ProblemCalculator();
		calculator.calculate("c:/funcs_6.txt", "src/in1.txt", "c:/out_6_hash.txt");
		
		/*
		for(Long val :calculator.generateRandomLongs(230)) {
			System.out.print(val.toHexString(val).toUpperCase() + " ");
		}
		*/
	}

}
