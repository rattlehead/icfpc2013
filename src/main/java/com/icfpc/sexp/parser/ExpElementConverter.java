package com.icfpc.sexp.parser;

import java.util.HashMap;
import java.util.Map;

import com.icfpc.sexp.element.AtomExpElement;
import com.icfpc.sexp.element.ExpElement;
import com.icfpc.sexp.element.VariableAtomExpElement;
import com.icfpc.sexp.parser.operations.*;

public class ExpElementConverter {
	private static ExpElement plus = new PlusExpOperation();
	private static ExpElement or = new OrExpOperation();
	private static ExpElement and = new AndExpOperation();
	private static ExpElement xor = new XorExpOperation();
	
	private static ExpElement not = new NotExpOperation();
	private static ExpElement shl1 = new Shl1ExpOperation();
	private static ExpElement shr1 = new Shr1ExpOperation();
	private static ExpElement shr4 = new Shr4ExpOperation();
	private static ExpElement shr16 = new Shr16ExpOperation();
	
	private static ExpElement ifOp = new IfExpOperation();
	private static ExpElement fold = new FoldExpElement();
	
	
	@SuppressWarnings("serial")
	private static Map<String, ExpElement> operationNameMap = new HashMap<String, ExpElement>() {
		{
			put(plus.toString(), plus);
			put(or.toString(), or);
			put(and.toString(), and);
			put(xor.toString(), xor);
			
			put(not.toString(), not);
			put(shl1.toString(), shl1);
			put(shr1.toString(), shr1);
			put(shr4.toString(), shr4);
			put(shr16.toString(), shr16);
			
			put(ifOp.toString(), ifOp);
			
			put(fold.toString(), fold);
		}
	};
	
	public ExpElement convert(String element) {
		if(operationNameMap.containsKey(element)) {
			return operationNameMap.get(element);
		} else if (element.charAt(0) >= 'a' && element.charAt(0) <= 'z') {
			return new VariableAtomExpElement(element);
		} else {
			Long value = Long.parseLong(element);
			return new AtomExpElement<Long>(value);
		}
	}
}
