package com.icfpc.sexp.parser.operations;

import com.icfpc.sexp.element.AbstractExpElement;

public class AndExpOperation extends AbstractExpElement {

	public int elementsAmount() {
		return 2;
	}

	public long evalIfGoodX(long[] x) {
		return x[0] & x[1];
	}

	public boolean isFunc() {
		return false;
	}

	public String toString() {
		return "and";
	}
	
}
