package com.icfpc.sexp.parser.operations;

import java.util.List;
import java.util.Map;

import com.icfpc.sexp.element.AbstractExpElement;
import com.icfpc.sexp.element.ExpElement;
import com.icfpc.sexp.parser.ExpSubtree;

public class FoldExpElement extends AbstractExpElement{
	// first child is fold
	private List<ExpElement> children;
	private Map<String, Long> valueMap;
	private long e0;
	private long e1;
	private String arg1;
	private String arg2;
	private Long prevValueArg1 = null;
	private Long prevValueArg2 = null;

	@Override
	public int elementsAmount() {
		return 0;
	}

	@Override
	public long evalIfGoodX(long[] x) {
		e0 = calculateE0(x);
		e1 = calculateE1(x);
		int i = 0;
		while(i < 8) {			
			ExpElement expToEval = addLambdaArgToMap();
			expToEval.setValueMap(valueMap);
			long result = expToEval.eval(x);
			e1 = result;
			e0>>>=8;
			removeLambdaArgFromMap();
			++i;
		}
		return e1;
	}

	private void removeLambdaArgFromMap() {
		if(prevValueArg1 != null) {
			valueMap.put(arg1, prevValueArg1);
		} else {
			valueMap.remove(arg1);
		}
		if(prevValueArg2 != null) {
			valueMap.put(arg2, prevValueArg2);
		} else {
			valueMap.remove(arg2);
		}
	}

	private ExpElement addLambdaArgToMap() {
		ExpSubtree lambdaSub = (ExpSubtree)children.get(3);
		arg1 = ((ExpSubtree)lambdaSub.getChildren().get(1)).getChildren().get(0).toString();
		arg2 = ((ExpSubtree)lambdaSub.getChildren().get(1)).getChildren().get(1).toString();
		prevValueArg1 = valueMap.get(arg1);
		prevValueArg2 = valueMap.get(arg2);
		valueMap.put(arg1, e0 & 0X00000000000000FF);
		valueMap.put(arg2, e1);
		return lambdaSub.getChildren().get(2);
	}

	private long calculateE0(long[] x) {
		ExpElement child = children.get(1);
		child.setValueMap(valueMap);
		return child.eval(x);
	}

	private long calculateE1(long[] x) {
		ExpElement child = children.get(2);
		child.setValueMap(valueMap);
		return child.eval(x);
	}

	public void setChilds(List<ExpElement> childs) {
		this.children = childs;
	}

	public void setValueMap(Map<String, Long> valueMap) {
		this.valueMap = valueMap;
	}
	
	public String toString() {
		return "fold";
	}
}
