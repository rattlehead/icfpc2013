package com.icfpc.sexp.parser.operations;

import com.icfpc.sexp.element.AbstractExpElement;

public class Shr4ExpOperation extends AbstractExpElement {

	public int elementsAmount() {
		return 1;
	}

	public long evalIfGoodX(long[] x) {
		return x[0]>>>4;
	}

	public boolean isFunc() {
		return false;
	}

	public String toString() {
		return "shr4";
	}
	

}
