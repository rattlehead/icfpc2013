package com.icfpc.sexp.parser.operations;

import com.icfpc.sexp.element.AbstractExpElement;

public class IfExpOperation extends AbstractExpElement {

	public int elementsAmount() {
		return 3;
	}

	public long evalIfGoodX(long[] x) {
		return x[0] == 0L ? x[1] : x[2];
	}

	public boolean isFunc() {
		return false;
	}

	public String toString() {
		return "if0";
	}

}
