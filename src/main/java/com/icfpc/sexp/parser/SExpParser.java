package com.icfpc.sexp.parser;

import com.icfpc.sexp.element.ExpElement;

public class SExpParser {
	private ExpElementConverter converter = new ExpElementConverter();

    private static final char SUBTREE_START = '(';

    private static final char SUBTREE_END = ')';

    public ExpElement parse(String expression) {
        return parseNextElement(expression, 0).element;
    }

    private ParserResponse parseNextElement(String expression, int startPos) {
        int expLen = expression.length();

        int i = startPos;
        while (i < expLen && Character.isWhitespace(expression.charAt(i)))
            i++;

        if (i >= expLen) {
            return new ParserResponse(null, expLen);
        }

        char c = expression.charAt(i);

        if (c == SUBTREE_START) {
            ExpSubtree subtree = new ExpSubtree();

            ParserResponse response = null;

            i++;
            do {
                response = parseNextElement(expression, i);
                if (response == null) {
                    throw new RuntimeException("Unable to parse expression " + expression + " starting at " + i);
                }

                if (response.element != null) {
                    subtree.addChild(response.element);
                }
                i = response.nextUnprocessedCharIndex;

            } while (i < expLen && response.element != null);

            return new ParserResponse(subtree, i);
        } else if (c == SUBTREE_END) {
            return new ParserResponse(null, i + 1);
        } else {
            StringBuilder atom = new StringBuilder();
            while (i < expLen && isAtomChar(expression.charAt(i))) {
                atom.append(expression.charAt(i));
                i++;
            }
            return new ParserResponse(converter.convert((atom.toString())), i);
        }
    }

    private boolean isAtomChar(char c) {
        return !Character.isWhitespace(c) && c != SUBTREE_START && c != SUBTREE_END;
    }

    private static class ParserResponse {

        public ParserResponse(ExpElement element, int nextUnprocessedCharIndex) {
            this.element = element;
            this.nextUnprocessedCharIndex = nextUnprocessedCharIndex;
        }

        ExpElement element;

        int nextUnprocessedCharIndex;
    }
}
