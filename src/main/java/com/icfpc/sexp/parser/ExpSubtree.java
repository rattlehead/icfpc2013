package com.icfpc.sexp.parser;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.icfpc.converter.HexToDecimalConverter;
import com.icfpc.sexp.element.ExpElement;
import com.icfpc.sexp.parser.operations.FoldExpElement;

public class ExpSubtree implements ExpElement {
	private Log log = LogFactory.getLog(ExpSubtree.class);
	private Map<String, Long> valueMap;
	private List<ExpElement> children = new LinkedList<ExpElement>();

	public ExpSubtree() {
	}

	public ExpSubtree(List<ExpElement> children) {
		setChildren(children);
	}

	public List<ExpElement> getChildren() {
		return children;
	}

	public void setChildren(List<ExpElement> children) {
		this.children = children;
	}

	public void addChild(ExpElement child) {
		children.add(child);
	}

	public String toString() {
		return "(" + StringUtils.join(children, " ") + ")";
	}

	public int elementsAmount() {
		return children.size();
	}

	// P ::= "(" "lambda" "(" id ")" e ")" -> (e)
	public String processProgramArg() {
		ExpElement arg = ((ExpSubtree) children.get(1)).children.get(0); // remove
																			// arg
																			// subtree
		ExpElement program = children.get(2);
		children.clear();
		children.add(program);
		return arg.toString();
	}

	public long eval(long[] x) {
		long result = 0;
		if (children.isEmpty()) {
			throw new RuntimeException("tree is empty");
		} else if (children.size() == 1) {
			children.get(0).setValueMap(valueMap);
			result = children.get(0).eval(x);
		} else if (children.get(0) instanceof FoldExpElement) {
			children.get(0).setValueMap(valueMap);
			((FoldExpElement) children.get(0)).setChilds(children);
			result = children.get(0).eval(x);
		} else { // the fist kid is an operation
			long[] childValues = computeChilds(x);
			result = children.get(0).eval(childValues);
		}
		/*
		if (log.isInfoEnabled()) {
			log.info("Eval subtree: " + children + ". Input param:" + valueMap);
			log.info("Result = " + result + ". Hex="
					+ HexToDecimalConverter.convertToHex(result));
		}
		*/
		return result;
	}

	private long[] computeChilds(long[] x) {
		long[] childValues = new long[children.size() - 1];
		ListIterator<ExpElement> iterator = children.listIterator(1);
		int i = 0;
		while (iterator.hasNext()) {
			ExpElement child = iterator.next();
			child.setValueMap(valueMap);
			childValues[i++] = child.eval(x);
		}
		return childValues;
	}

	public void setValueMap(Map<String, Long> valueMap) {
		this.valueMap = valueMap;
	}
}
