package com.icfpc.converter;

import java.math.BigInteger;

public class HexToDecimalConverter {
	public static Long convertToDec(String hex) {
		if(hex.charAt(0)== '0' && hex.length() > 1) {
			hex = hex.substring(2);
		}
		BigInteger bigInt = new BigInteger(hex, 16);
		return bigInt.longValue();
	}
	
	public static String convertToHex(Long dec) {
		String value = Long.toHexString(dec).toUpperCase();
		while(value.length() < 16) {
			value = "0" + value;
		}
		return "0x" + value;
	}
}
