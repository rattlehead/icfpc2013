package com.icfpc.json;

public class Problem {
	String id;
	int size;
	String[] operators;
	boolean status;
	
	public Problem() {}
	
	public Problem(String id, String[] operators, int size) {
		super();
		this.id = id;
		this.size = size;
		this.operators = operators;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String[] getOperators() {
		return operators;
	}

	public void setOperators(String[] operators) {
		this.operators = operators;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
}
