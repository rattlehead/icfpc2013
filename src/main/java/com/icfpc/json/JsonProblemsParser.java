package com.icfpc.json;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonProblemsParser {
	public List<Problem> parse(String fileName) throws FileNotFoundException,
			ParseException {
		Scanner sc = new Scanner(new File(fileName));
		StringBuilder sb = new StringBuilder();
		List<Problem> result = new ArrayList<Problem>();
		while (sc.hasNext()) {
			sb.append(sc.nextLine());
		}

		JSONParser parser = new JSONParser();
		Object objArray = parser.parse(sb.toString());
		JSONArray array = (JSONArray) objArray;
		for (Object o_ : array) {
			JSONObject o = (JSONObject) o_;
			String[] operators = getOperators(o.get("operators"));
			Problem problem = new Problem(o.get("id").toString(), operators,
					Integer.parseInt(o.get("size").toString()));
			if (o.get("status") == null || !o.get("status").equals("done")) {
				problem.setStatus(false);
			} else {
				problem.setStatus(true);
			}

			result.add(problem);
		}
		return result;
	}

	private String[] getOperators(Object object) {
		JSONArray ops = (JSONArray) object;
		String[] result = new String[ops.size()];
		int i = 0;
		for (Object op_ : ops) {
			result[i] = op_.toString();
			i++;
		}
		return result;
	}

	public static void main(String[] args) throws FileNotFoundException,
			ParseException {
		JsonProblemsParser parser = new JsonProblemsParser();
		List<Problem> problems = parser.parse("src/problems.json");
		//writeSizeWithFoldAmount(problems);
		writeSizeIdWithoutFolds(problems);

	}

	private static void writeSizeIdWithoutFolds(List<Problem> problems) {
		Map<Integer, List<String>> sizeProblemMap = new HashMap<Integer, List<String>>();
		for (final Problem problem : problems) {
			if (!containsFold(problem) && !problem.status) {
				if (sizeProblemMap.containsKey(problem.getSize())) {
					sizeProblemMap.get(problem.getSize()).add(problem.getId());
				} else {
					sizeProblemMap.put(problem.getSize(),
							new LinkedList<String>() {
								{
									add(problem.getId());
								}
							});
				}
			}
		}

		for (Map.Entry<Integer, List<String>> entry : sizeProblemMap.entrySet()) {
			System.out.println(entry.getKey() + "\t" + entry.getValue());
		}
	}

	private static void writeSizeWithFoldAmount(List<Problem> problems) {
		Map<Integer, Integer> sizeAmount = new HashMap<Integer, Integer>();
		Map<Integer, Integer> withfolds = new HashMap<Integer, Integer>();
		for (Problem problem : problems) {
			if (containsFold(problem)) {
				if (withfolds.containsKey(problem.getSize())) {
					withfolds.put(problem.getSize(),
							withfolds.get(problem.getSize()) + 1);
				} else {
					withfolds.put(problem.getSize(), 1);
				}
			}
			if (sizeAmount.containsKey(problem.getSize())) {
				sizeAmount.put(problem.getSize(),
						sizeAmount.get(problem.getSize()) + 1);
			} else {
				sizeAmount.put(problem.getSize(), 1);
			}
		}

		for (Map.Entry<Integer, Integer> entry : sizeAmount.entrySet()) {
			System.out.println("Size:" + entry.getKey() + " Number:"
					+ entry.getValue() + " With folds:"
					+ withfolds.get(entry.getKey()));
		}
	}

	private static boolean containsFold(Problem problem) {
		for (String op : problem.operators) {
			if (op.contains("fold"))
				return true;
		}
		return false;
	}

	private static void writeSizeAmount(List<Problem> problems) {
		Map<Integer, Integer> sizeAmount = new HashMap<Integer, Integer>();
		for (Problem problem : problems) {
			if (sizeAmount.containsKey(problem.getSize())) {
				sizeAmount.put(problem.getSize(),
						sizeAmount.get(problem.getSize()) + 1);
			} else {
				sizeAmount.put(problem.getSize(), 1);
			}
		}

		for (Map.Entry<Integer, Integer> entry : sizeAmount.entrySet()) {
			System.out.println("Size:" + entry.getKey() + " Number:"
					+ entry.getValue());
		}
	}
}
