package com.icfpc;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.icfpc.sexp.element.ExpElement;
import com.icfpc.sexp.parser.ExpSubtree;
import com.icfpc.sexp.parser.SExpParser;

public class TestBootClass {
	static Log log = LogFactory.getLog(TestBootClass.class);

	public static void main(String[] args) {
		SExpParser parser = new SExpParser();
		String line = "(lamda(x) (fold x 0 (lambda (y z) (or y z))))";
		log.info("Parse next line: " + line);
		ExpElement tree = parser.parse(line);
		log.info(tree);
		String argName = "x";
		if(tree instanceof ExpSubtree) {
			argName = ((ExpSubtree) tree).processProgramArg();
		}
		HashMap<String, Long> valueMap = new HashMap<String, Long>();
		valueMap.put(argName, 1123L);
		tree.setValueMap(valueMap);
		log.info(tree.eval(new long[] {1123L}));
	}

}
