import random

from client import Client
from program import to_python, find_function, to_bv


def verify_values(c, task_id, sample_data):
  args, values = zip(*sample_data)
  payload = {
      'id': task_id,
      'arguments': map(lambda a: '0x%X' % a, args),
  }
  res = c.eval(payload)

  if res['status'] != 'ok':
    print 'error', res['message']
    return

  for server_value, our_value in zip(res['outputs'], values):
    assert int(server_value, 16) != values, (server_value, our_value)



def main():
  c = Client()
  train_data = c.train({'operators': [], 'size': 20})
  challenge = train_data['challenge']
  task_id = train_data['id']
  print 'Task id:', task_id
  print 'Challenge:', challenge
  operators = train_data['operators']
  size = train_data['size']
  print 'Challenge size:', size
  print 'Challenge operators:', operators

  sample_data = []
  fun = to_python(challenge)
  for i in range(255):
    arg = random.randint(0, 1<<64)
    sample_data.append((arg, fun(arg)))

  verify_values(c, task_id, sample_data)

  while True:
    print 'searching for function'
    with open('/tmp/qq', 'w') as w:
      w.write(str(size) + "\n")
      for a, b in sample_data:
        w.write("%s %s\n" % (a, b))
    f = find_function(size, sample_data, operators)

    if f:
      print 'Found f:', f
      response = c.guess({'id': task_id, 'program': to_bv(f)})
      print 'Response:', response
      if response['status'] == 'mismatch':
        input, server_value, our_value = [int(x, 16) for x in
                                          response['values']]
        if (to_python(f)(input) != our_value):
          import ipdb;ipdb.set_trace()
        print 'Appending new datapoint and retrying.'
        sample_data.append((input, server_value))
      elif response['status'] == 'win':
        break

    else:
      print 'not found'





if __name__ == '__main__':
  while True:
    main()
