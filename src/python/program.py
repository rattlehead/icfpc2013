import time
import random
import copy

from pprint import pprint
from pyparsing import *

id = Word(alphanums + '_')#.setResultsName('id')

op1 = (Literal("not") | Literal("shl1") | Literal("shr16") | Literal("shr4") |
       Literal("shr1"))#.setResultsName('op1')
op2 = (Literal("and") | Literal("or") | Literal("xor") |
       Literal("plus"))#.setResultsName('op2')
e = Forward()
e << (Literal("0") | Literal("1") | id
     | Group(Suppress("(") + Literal("if0") + e + e + e + Suppress(")"))
     | Group(Suppress("(") + Literal("fold") + e + e + Group(Suppress("(") +
                                                             Literal("lambda") +
                                                             Suppress("(") + id
                                                             + id +
                                                             Suppress(")")
                                                             + e + Suppress(")")) + Suppress(")"))
     | Group(Suppress("(") + op1 + e + Suppress(")"))
     | Group(Suppress("(") + op2 + e + e + Suppress(")")))#.setResultsName('e')
program = (Suppress("(") + Literal("lambda") + Suppress("(") + id +
           Suppress(")") + e + Suppress(")"))#.setResultsName('program')

op1s = frozenset(["not", "shl1", "shr1", "shr4", "shr16"])
op2s = frozenset(["and", "or", "xor", "plus"])

def size(item):
  """Computes size of the program.

  Args:
    item: parse tree of the program.
  """
  if item in ['0', '1']:
    return 1
  elif item[0] == 'if0':
    return 1 + sum(map(size, item[1:]))
  elif item[0] == 'fold':
    lam = item[3]
    return 2 + sum(map(size, item[1:3])) + size(lam[3])
  elif item[0] in op1s:
    return 1 + size(item[1])
  elif item[0] in op2s:
    return 1 + sum(map(size, item[1:]))
  elif item[0] == 'lambda':
    return 1 + size(item[-1])
  else: # id
    return 1

def get_operators(item, ops=None):
  if ops is None:
    ops = set()
  if item[0] == 'lambda':
    get_operators(item[-1], ops)
  elif item[0] in op1s | op2s | {'if0', 'fold'}:
    ops.add(item[0])
    for i in item[1:]:
      get_operators(i, ops)

  return ops


def fold(x, acc, fun):
  """Implemets \BV fold."""
  for i in range(8):
    acc = fun(x & 255, acc)
    x = x >> 8
  return acc

def rshift(val, n):
  if isinstance(val, basestring):
    print 'bacd val',val
  return (val % 0x10000000000000000) >> n

op2_map = {
    'plus': '+',
    'xor': '^',
    'and': '&',
    'or': '|',
}

def op2_action(item):
  return '(%s %s %s)' % (comp(item[1]), op2_map[item[0]], comp(item[2]))

def if0_action(item):
  return '(%s if %s==0 else %s)' % tuple(map(comp, [item[2], item[1], item[3]]))

def fold_action(item):
  return  'fold(%s, %s, %s)' % (comp(item[1]), comp(item[2]), comp(item[3]))

def not_action(item):
  return '(~%s & ((1<<64)-1))' % comp(item[1])

def shl1_action(item):
  return '((%s << 1) %% (1<<64))' % comp(item[1])

def shr1_action(item):
  return 'rshift(%s, 1)' % comp(item[1])

def shr4_action(item):
  return 'rshift(%s, 4)' % comp(item[1])

def shr16_action(item):
  return 'rshift(%s, 16)' % comp(item[1])

def lambda_action(item):
  return '(lambda %s: %s)' % (', '.join(item[1:-1]), comp(item[-1]))

comp_map = {
  'if0': if0_action,
  'fold': fold_action,
  'not': not_action,
  'shl1': shl1_action,
  'shr1': shr1_action,
  'shr4': shr4_action,
  'shr16': shr16_action,
  'plus': op2_action,
  'xor': op2_action,
  'and': op2_action,
  'or': op2_action,
  'lambda': lambda_action,
}

def ident(x): return x

def comp_short(item):
  """Interprets \BV parse tree into a python expression.

  Args:
    item: parse tree

  Returns:
    string python code for a lambda that executes the program.
  """
  if item in ['0', '1']:
    return int(item)

  return comp_map.get(item[0], ident)(item)

def comp_long(item):
  """Interprets \BV parse tree into a python expression.

  Args:
    item: parse tree

  Returns:
    string python code for a lambda that executes the program.
  """
  if item in ['0', '1']:
    return int(item)
  elif item[0] == 'if0':
    return '(%s if %s==0 else %s)' % tuple(map(comp, [item[2], item[1],
                                                            item[3]]))
  elif item[0] == 'fold':
    return 'fold(%s, %s, %s)' % (comp(item[1]), comp(item[2]), comp(item[3]))

  elif item[0] == 'not':
    return '(~%s)' % comp(item[1])
  elif item[0] == 'shl1':
    return '(%s << 1)' % comp(item[1])
  elif item[0] == 'shr1':
    return 'rshift(%s, 1)' % comp(item[1])
  elif item[0] == 'shr4':
    return 'rshift(%s, 4)' % comp(item[1])
  elif item[0] == 'shr16':
    return 'rshift(%s, 16)' % comp(item[1])

  elif item[0] in op2s:
    return '(%s %s %s)' % (comp(item[1]), op2_map.get(item[0], item[0]),
                           comp(item[2]))
  elif item[0] == 'lambda':
    return '(lambda %s: %s)' % (', '.join(item[1:-1]), comp(item[-1]))
  else:
    return item

comp = comp_short

def to_python(pr):
  """Converts a program to python representation.

  Args:
    pr: either string \BV program representation or a parse tree

  Returns:
    python lambda that executes the program.
  """
  if isinstance(pr, basestring):
    pr = program.parseString(pr)

  return eval(comp(pr))


def to_bv(item):
  """Serializes parse tree into \BV program."""
  if item in ['0', '1']:
    return item
  if item[0] == 'if0':
    return '(if0 %s %s %s)' % (to_bv(item[1]), to_bv(item[2]), to_bv(item[3]))
  if item[0] == 'fold':
    return '(fold %s %s %s)' % (to_bv(item[1]), to_bv(item[2]), to_bv(item[3]))
  if item[0] in op1s | op2s:
    return '(%s %s)' % (item[0], ' '.join(map(to_bv, item[1:])))
  if item[0] == 'lambda':
    return '(lambda (%s) %s)' % (' '.join(map(to_bv, item[1:-1])),
                                 to_bv(item[-1]))
  return item


def find_function(max_size, data_points, operators, do_optimize=True):
  """Tries to find a function that matches data points.

  Args:
    max_size: max program size to consider.
    data_points: list of (arg, value) that must be f(arg)=value for taget
      function.
    operators: TODO() this is not used yet. This limits the possible operators.
  Returns:
    parse tree of matched function
    """
  last_time = time.time()
  c = 0
  for f in generate_program(max_size, operators=operators):
    if c % 1000 == 0:
      print 'finding function: %s, %s' % (c, time.time()-last_time)
      last_time = time.time()
    if c == 10000000:
      return None
    c += 1
    #if size(f) != target_size:
    #  continue
    fun = eval(comp(f))
    for inp, out in data_points:
      if fun(inp) != out:
        break
    else:
      if do_optimize:
        optimize(f)
      return f
  else:
    return None


def optimize(pr):
  """Optimizes the program parse tree.
  >>> optimize('atom')
  'atom'
  >>> optimize(['not', ['not', ['shl1', '0']]])
  ['shl1', '0']

  """
  if isinstance(pr, basestring):
    return pr

  changed = False
  if pr[0] == 'not':
    if pr[1][0] == 'not':
      pr[:] = optimize(pr[1][1])
      changed = True

  for i, p in enumerate(pr[:]):
    changed = optimize(p)
    if changed:
      pr[i] = changed
      changed = True

  if changed:
    return pr
  else:
    return None


def main():
  #P_str = '(lambda (x_5071) (fold x_5071 0 (lambda (x_5071 x_5072) (shr4 (or x_5071 x_5071)))))'

  P_str = '''(lambda (x) (xor (shr1 x)
                                  (shr4 (not (plus (shr1 (if0 (shr1 (xor (and (shr16 x)
                                                                              (shr4 x))
                                                                     0))
                                                              x
                                                              0))
                                                    x)))))'''

  P_str = '(lambda (x_10164) (if0 (plus (and (shr4 x_10164) 1) 0) x_10164 1))'
  P_str = '(lambda (x) (shr16 x))'
  P_str = '(lambda (x) (fold x 1 (lambda (a b) (plus (not a) b))))'
  P_str = '(lambda (x) (fold x 0 (lambda (a b) (plus a b))))'

  print 'parsing following string'
  print P_str
  print '-----------'
  #import ipdb;ipdb.set_trace()
  pr = program.parseString(P_str)
  print 'parse tree'
  print pr
  print '-----------'
  print 'program size'
  print size(pr)
  print '-----------'
  print 'compiled to python'
  print comp(pr)
  fun = eval(comp(pr))

  target_outputs = [(x, fun(x)) for x in [0, 1, 100, 1<<10, 1<<20, (1<<20)-1,
                                          (1<<28)-1]]
  print '-----------'
  print 'looking for a function for following input-output:'
  print target_outputs
  target_size = 10

  print '-----------'
  print 'of size', target_size

  fun = find_function(target_size, target_outputs, [])
  if fun:
    print fun
  else:
    print 'not found'


expressions_cache = {}

def generate_expressions(max_size, free_vars, operators):
  cache_key = (max_size, str(sorted(free_vars)), str(sorted(operators)))
  if cache_key in expressions_cache:
    for x in expressions_cache[cache_key]:
      yield x
    return

  cache_value = []
  for x in _generate_expressions(max_size, free_vars, operators):
    cache_value.append(x)
    yield x
  expressions_cache[cache_key] = cache_value

def _generate_expressions(max_size, free_vars, operators):
  """Generates all possible parse trees of the given size.

  Args:
    max_size: max size of a tree to generate.
    free_vars: set of free variable names that can be used as atoms in a subtree

  Return:
    parse tree fragment
  """
  assert max_size
  if free_vars is None:
    free_vars = set()
  yield '0'
  yield '1'
  for x in free_vars:
    yield x

  if max_size == 1:
    return

  for op in op1s:
    if op in operators:
      for arg in generate_expressions(max_size-1,
                                      free_vars,
                                      operators):
        yield [op, arg]

  if max_size == 2:
    return

  for op in op2s:
    if op in operators:
      for arg1_size in range(1, max_size-1):
        arg2_size = max_size - 1 - arg1_size
        for arg1 in generate_expressions(arg1_size,
                                        free_vars,
                                         operators):
          for arg2 in generate_expressions(arg2_size,
                                          free_vars,
                                           operators):
            yield [op, arg1, arg2]
  if max_size == 3: return

  # if0
  if 'if0' in operators:
    for arg1_size in range(1, max_size-2):
      for arg2_size in range(1, max_size-1-arg1_size):
        arg3_size = max_size - 1 - arg2_size - arg1_size
        for arg1 in generate_expressions(arg1_size,
                                         free_vars,
                                         operators):
          for arg2 in generate_expressions(arg2_size,
                                           free_vars,
                                           operators):
            for arg3 in generate_expressions(arg3_size,
                                             free_vars,
                                             operators):
              yield ['if0', arg1, arg2, arg3]

  if max_size == 4: return

  # fold
  if set(['fold', 'tfold']) ^ operators:
    for arg1_size in range(1, max_size-1):
      for arg2_size in range(1, max_size-1-arg1_size):
        arg3_size = max_size - arg2_size - arg1_size
        for arg1 in generate_expressions(arg1_size,
                                         free_vars,
                                         operators):
          for arg2 in generate_expressions(arg2_size,
                                           free_vars,
                                           operators):
            for lam in generate_lambda(arg3_size,
                                       free_vars,
                                       operators):
              yield ['fold', arg1, arg2, lam]

lambda_cache = {}

all_operators = op1s | op2s | set(['if0', 'fold'])
def generate_lambda(max_size, free_vars, operators):
  cache_key = (max_size, str(sorted(free_vars)))
  if cache_key in lambda_cache:
    for x in lambda_cache[cache_key]:
      yield x
    return

  cache_value = []
  for x in _generate_lambda(max_size, free_vars, operators):
    cache_value.append(x)
    yield x
  lambda_cache[cache_key] = cache_value

def _generate_lambda(max_size, free_vars, operators):
  """Generates all possible lambdas of given size as a parse subtree."""
  new_var_names = []
  while len(new_var_names) < 2:
    rand_int = random.randint(1, 1000)
    var_name = 'y_%s' % rand_int
    if var_name in free_vars or var_name in new_var_names:
      continue
    new_var_names.append(var_name)

  free_vars = copy.copy(free_vars)
  free_vars |= set(new_var_names)

  for expr in generate_expressions(max_size-1, free_vars, operators):
    yield ['lambda', new_var_names[0], new_var_names[1], expr]


def generate_program(target_size, operators):
  """Generates all possible programs of a given size."""
  var_name = 'x'
  free_vars = set([var_name])
  if 'tfold' not in operators:
    for expr in generate_expressions(target_size, free_vars, all_operators):
      yield ['lambda', 'x', expr]
  else:
    operators = frozenset(operators - set(['tfold']))
    for expr in generate_expressions(target_size, free_vars, all_operators):
      yield ['lambda', 'x', ['fold', 'x', '0', ['lambda', 'x', 'y', expr]]]

def tests():
  cases = {
    '(lambda (y) 0)': [(0, 0), (1, 0), (100, 0)],
    '(lambda (y) 1)': [(0, 1), (1, 1), (100, 1)],
    '(lambda (y) (not y))': [(1,   ~1   & ((1<<64)-1)),
                             (5,   ~5   & ((1<<64)-1)),
                             (100, ~100 & ((1<<64)-1)),
                             (0,   ~0   & ((1<<64) - 1))],
    '(lambda (y) (if0 0 y (not y)))': [(1, 1), (5, 5), (100, 100), (0, 0)],
    #'(lambda (y) (if0 1 y (not y)))': [(1, ~1), (5, ~5), (100, ~100), (0, ~0)],
    '(lambda (y) (fold y 0 (lambda (a b) (plus a b))))': [(0, 0), (1, 1), (3, 3),
                                                          (0x101, 2), (0x103, 4)],
    '''(lambda (x_27553) (xor (shr1 x_27553) (shr4 (not (plus (shr1 (if0 (shr1
    (xor (and (shr16 x_27553) (shr4 x_27553)) 0)) x_27553 0)) x_27553)))))''':
      [(0, rshift((~0), 4))],
    '''(lambda (x_27458) (or (shr4 (shl1 (shr1 (shl1 (shr1 (shl1 (not (or (if0
    (shr4 (shr16 (xor x_27458 0))) 0 x_27458) 0)))))))) x_27458))''': [
        (0x38E38, 0x0FFFFFFFFFFFFFFF)],
    '(lambda (x) (shl1 x))': [(18200423424826954441, 17954102775944357266)],
    '(lambda (x) (not x))': [(0, 18446744073709551615L)],
  }
  for p_str, values in cases.items():
    pr = program.parseString(p_str)
    fun = eval(comp(pr))
    for arg, value in values:
      assert fun(arg) == value, (p_str, arg, value)

if __name__ == '__main__':
  import doctest
  doctest.testmod()
  tests()
  main()
