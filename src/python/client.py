import functools
import time
import json
import threading
from pprint import pprint

import requests

class ProblemExpired(Exception):
  pass

class ProblemSolved(Exception):
  pass

class Client(object):
  AUTH_TOKEN = '0385W2ezdcbS4BfqJwcGvQ283U28UK0GcDTcn3sd'
  BASE_URL = 'http://icfpc2013.cloudapp.net/%%s?auth=%svpsH1H' % AUTH_TOKEN

  _REQUEST_LIMIT_CACHE_TIME = 900
  _request_window_ts = None
  _request_window = None

  _request_limit_ts = None
  _request_limit = None

  def __init__(self):
    self._lock = threading.RLock()

  #def _update_request_window(self):
  #  status = self.status(ratelimit=False)
  #  print status["contestScore"]
  #  self._request_window = status['requestWindow']
  #  self._request_window_ts = time.time()
  #  self._request_limit = self._request_window['limit']
  #  self._request_limit_ts = time.time()

  #@property
  #def request_window(self):
  #  if self._request_window is None:
  #    self._update_request_window()

  #  reset_at = self._request_window['resetsIn'] + self._request_window_ts
  #  if reset_at < time.time():
  #    self._update_request_window()

  #  return self._request_window


  def wait_for_window(self):
    # Allow initial status request.
    return
    #if not self._request_window:
    #  self._update_request_window()
    #limit = self.request_limit
    #window = self.request_window

  #  if window['amount'] < limit:
  #    self._request_window['amount'] += 1
  #    return
  #  else:
  #    resets_at = self._request_window_ts + self.request_window['resetsIn']
  #    time_left = resets_at - time.time()

  #    print 'Sleeping for', time_left
  #    time.sleep(time_left)
  #    self._request_window['amount'] += 1

  #@property
  #def request_limit(self):
  #  if (self._request_limit is None or
  #      time.time() - self._request_limit_ts > self._REQUEST_LIMIT_CACHE_TIME):
  #    self._update_request_window()
  #  return self._request_limit

  def call_method(self, name, data=None, ratelimit=True, timeout=1):
    with self._lock:
      #if ratelimit:
      #  self.wait_for_window()
      r = requests.post(self.BASE_URL % name, data=json.dumps(data))
      try:
        return r.json()
      except Exception:
        print 'Response code:', r.status_code
        print r.text
        if r.status_code == 410:
          raise ProblemExpired()
        if r.status_code == 429:
          timeout = min(timeout * 2, 20)
          self.wait_for_window()
          print 'Sleeping for %s' % timeout
          time.sleep(timeout)
          return self.call_method(name, data, ratelimit, timeout)
        if r.status_code == 412:
          raise ProblemSolved()
        else:
          raise

  def make_api_method(name):
    def method(self, *args, **kwargs):
      return self.call_method(name, *args, **kwargs)
    return method

  def api_method(self, name):
    return getattr(self, name)
  status = make_api_method('status')
  train = make_api_method('train')
  myproblems = make_api_method('myproblems')
  eval = make_api_method('eval')
  guess = make_api_method('guess')


def main():
  c = Client()
  for method in ['status']*7: # 'myproblems']:
    print method
    pprint(c.api_method(method)())

if __name__ == '__main__':
  main()
