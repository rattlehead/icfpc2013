import subprocess

import submitter
import program

TEST_CASES = [('(lambda (x) 0)', [(0, 0), (1, 0), (100, 0)]),
              ('(lambda (x) 1)', [(0, 1), (1, 1), (100, 1)]),
              ('(lambda (x) (not x))', [(1,   ~1   & ((1<<64)-1)),
                                        (5,   ~5   & ((1<<64)-1)),
                                        (100, ~100 & ((1<<64)-1)),
                                        (0,   ~0   & ((1<<64)-1))]),
              ('(lambda (x) (if0 0 x (not x)))', [(1, 1), (5, 5), (100, 100), (0, 0)]),
              #('(lambda (x) (if0 1 x (not x)))', [(1, ~1), (5, ~5), (100, ~100), (0, ~0)],
              ('(lambda (x) (fold x 0 (lambda (a b) (plus a b))))',
               [(0, 0), (1, 1), (3, 3), (0x101, 2), (0x103, 4)]),
              ('(lambda (x_10515) (plus (if0 (shr1 (and (shl1 x_10515) '
               'x_10515)) 0 x_10515) 1))', []),
              ('(lambda (x_27553) (xor (shr1 x_27553) (shr4 (not (plus (shr1 '
               '(if0 (shr1 (xor (and (shr16 x_27553) (shr4 x_27553)) 0)) '
               'x_27553 0)) x_27553)))))',
               [(0, program.rshift((~0), 4))]),
              #('''(lambda (x_27458) (or (shr4 (shl1 (shr1 (shl1 (shr1 (shl1 (not (or (if0
              # (shr4 (shr16 (xor x_27458 0))) 0 x_27458) 0)))))))) x_27458))''', [
              #     ((0x38E38, 0x0FFFFFFFFFFFFFFF))]),
              ('(lambda (x) (shl1 x))', [(18200423424826954441, 17954102775944357266)]),
              ('(lambda (x) (not x))', [(0, 18446744073709551615L)]),
              ('(lambda (x_4444) (xor x_4444 (shr4 (not x_4444))))', []),
              ]


class ExternalSolverTester(object):
  def test(self):
    for pr, some_points in TEST_CASES:
      f = program.program.parseString(pr)
      fun = program.to_python(f)
      inputs = submitter.generate_inputs(10)

      outputs = map(lambda inp: fun(inp) % (1<<64), inputs)

      self.data_points = zip(inputs, outputs) + some_points
      self.operators = program.get_operators(f)
      self.size = program.size(f)

      print 'Test case:', pr
      for l in self._stdin_generator():
        print l,
      solution = self.solve()

      if not solution:
        print 'Test case:', pr
        for l in self._stdin_generator():
          print l,

        assert False, "No solution"
      fun = program.to_python(solution)
      solution_outputs = map(fun, inputs)
      if solution_outputs != outputs:
        print 'Test case:', pr
        print 'Solution:', solution
        #print self.data_points
        assert solution == pr


  def _stdin_generator(self):
    yield str(self.size) + "\n"
    yield ' '.join(self.operators) + "\n"
    for point in self.data_points:
      yield "0x%X 0x%X\n" % point

  def _call_cmd(self, cmd_args):
    cmd = self.CMD + cmd_args
    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    for line in self._stdin_generator():
      p.stdin.write(line)
    p.stdin.close()
    output = p.stdout.read().strip()
    return output

  def solve(self):
    solution = self._call_cmd([])
    return solution

class GoSolverTest(ExternalSolverTester):
  CMD = ['../go/run.sh']


class PythonSolverTest(ExternalSolverTester):
  def solve(self):
    return program.to_bv(program.find_function(self.size, self.data_points,
                                               self.operators))




def main():
  GoSolverTest().test()
  #PythonSolverTest().test()

if __name__ == '__main__':
  main()
