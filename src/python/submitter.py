import os
import subprocess
import time
import json
import logging
import random
from collections import namedtuple
import sqlite3

import threading
import Queue
from client import Client, ProblemExpired, ProblemSolved
from program import find_function, to_bv, to_python, program, size

logging.basicConfig()


Task = namedtuple('Task', 'data_points id task_size operators, program')
SIZE = 30
#OPERATORS = ["tfold"]
OPERATORS = []
NUM_SOLVER_WORKERS = 5
REAL_SOLVER_CUTOFF = 10


log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

class DbMultiplexor(threading.Thread):
  def __init__(self):
    super(DbMultiplexor, self).__init__()
    self.daemon = True
    self.q = Queue.Queue()

  def _setup_db(self):
    self.db = sqlite3.connect('tasks.db')

    self.db.execute('''CREATE TABLE IF NOT EXISTS task_state (
        task_id text,
        task_state text,
        solution text)''')
    self.db.commit()

  def __getattr__(self, method):
    def proxy(*args, **kwargs):
      def closure():
        res = getattr(self.db, method)(*args, **kwargs)
        if method == 'execute':
          res = list(res)
          return res

      closure.done = threading.Event()
      self.q.put(closure)
      closure.done.wait()
      return closure.res
    return proxy

  def run(self):
    self._setup_db()
    while True:
      cloure = self.q.get(block=True)
      res = cloure()
      self.q.task_done()
      cloure.res = res
      cloure.done.set()

db = DbMultiplexor()
db.start()


def generate_random_inputs(size=255):
  sample_args = [0, 1, 2]
  for i in range(size-3):
    sample_args.append(random.randint(0, 1<<64))
  return sample_args


def generate_java_inputs(size=256):
  fname = os.path.join(os.path.dirname(__file__), '..', 'in1.txt')
  res = map(lambda x: int(x, 16), open(fname).read().split())
  print len(res)
  return res

generate_inputs = generate_random_inputs
#generate_inputs = generate_java_inputs


def mark_task_done(task, solution):
  if task.id:
    db.execute('UPDATE task_state SET task_state = "done", solution=? WHERE task_id =?',
               [solution, task.id])
    db.commit()


class SolverWorker(threading.Thread):
  _next_id = 1

  def __init__(self, q, client):
    super(SolverWorker, self).__init__()
    self.id = self._next_id
    self.__class__._next_id += 1
    self._log = logging.getLogger(__name__ + ' solver ' + str(self.id))
    self._log.setLevel(logging.INFO)
    self.q = q
    self.client = client
    self.finish = threading.Event()

  def log(self, msg, level=logging.INFO):
    self._log.log(level, msg)

  def run(self):
    while True:
      if self.finish.is_set():
        break
      task = self.q.get(block=True)
      try:
        if task is None:
          self.log('work done')
          break
        self.task = task

        self.log('starting work on %s' % task.id)
        print time.asctime()
        start_time = time.time()
        solution = self.solve()
        time_passed = time.time() - start_time
        self.log('found some solution in %s' % time_passed)

        if not solution:
          time_passed = None
          task_size = task.task_size
        else:
          task_size = size(program.parseString(solution))
        self.write_stats(task_size, time_passed, task.operators,
                         solution or task.program)

        if not solution:
          print "!!!!Solution not found"
          continue

        assert solution
        while True:
          try:
            result = self.check(solution)
          except IndexError:
            print solution
            raise
          if result['status'] == 'win':
            self.log('win: %s' % solution)
            self.mark_done(to_bv(solution))
            break
          elif result['status'] == 'mismatch':
            self.log('mismatch')
            self.add_point((int(result['values'][0], 16),
                            int(result['values'][1], 16)))
            solution = self.solve()
          elif result['status'] == 'error':
            self.log('error: %s' % result['message'], level=logging.ERROR)
            break
          else:
            assert False
      finally:
        self.q.task_done()

  def mark_done(self, solution):
    mark_task_done(self.task, solution)

  def check(self, solution):
    response = self.client.guess({'id': self.task.id, 'program': to_bv(solution)})
    return response

  def solve(self):
    """Returns a single solution for self.task as a parse tree."""
    raise NotImplementedError()

  def add_point(self, point):
    """Adds a point to the data set.

    Args:
      point: (arg, value) tuple
    Returns:
      None
    """
    raise NotImplementedError()

  def stop(self):
    self.finish.set()

  def write_stats(self, size, time, operators, program):
    with open('/tmp/stats', 'a') as s:
      s.write("%s %s %s %s\n" % (size, time, ' '.join(operators), program))



class PythonSolver(SolverWorker):
  def solve(self):
    print 'size', self.task.task_size
    print 'data', self.task.data_points[:3]
    print 'operators', self.task.operators
    return find_function(self.task.task_size,
                         self.task.data_points,
                         self.task.operators)

  def add_point(self, point):
    self.task.data_points.append(point)


class JavaSolver1(SolverWorker):
  JAVA_CMD = ['../../target/icfpc2013-1.0-distribution/bin/run.sh']
  funcs = None
  def solve(self):
    if self.funcs is None:
      outputs = zip(*self.task.data_points)[1]
      java_arg = ['../../src/out_7_hash.txt']
      java_stdin = ' '.join(map(lambda x: '0x%X'%x, outputs))

      self.funcs = self._call_java(java_arg, java_stdin)

    if self.funcs:
      return program.parseString(self.funcs.pop())

  def _call_java(self, java_arg, java_stdin):
    cmd = self.JAVA_CMD + java_arg
    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    p.stdin.write(java_stdin)
    p.stdin.close()
    output = p.stdout.read()
    funcs = output.splitlines()
    return funcs

  def add_point(*args, **kwargs):
    pass

class GoSolver(SolverWorker):
  CMD = ['../go/run.sh 2> /dev/null']

  def _stdin_generator(self):
    yield str(self.task.task_size) + "\n"
    yield ' '.join(self.task.operators) + "\n"
    for point in self.task.data_points:
      yield "0x%X 0x%X\n" % point

  def _call_cmd(self, cmd_args):
    cmd = self.CMD + cmd_args
    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
    with open('/tmp/qq', 'w') as w:
      for x in self._stdin_generator():
        w.write(x)
    for line in self._stdin_generator():
      p.stdin.write(line)
    p.stdin.close()
    output = p.stdout.read().strip()
    return output

  def solve(self):
    solution = self._call_cmd([])
    return solution

  def add_point(self, point):
    self.task.data_points.append(point)


class SubmitterBase(object):
  NUM_SOLVERS = NUM_SOLVER_WORKERS
  #SOLVER_CLASS = JavaSolver1
  #SOLVER_CLASS = PythonSolver
  SOLVER_CLASS = GoSolver

  def __init__(self):
    self.problems = Queue.Queue(1)
    self.client = Client()
    self.solvers = []

  def fetch_problems(self):
    raise NotImplementedError

  def start(self):
    for i in range(self.NUM_SOLVERS):
      s = self.SOLVER_CLASS(self.problems, self.client)
      self.solvers.append(s)
      s.daemon = True
      s.start()

    log.info('Started %s solvers', self.NUM_SOLVERS)

    for problem in self.fetch_problems():
      log.info('Got a problem: %s %s', problem.id, problem.task_size)
      if self.finished_problem(problem.id):
        log.info('KNOWN PROBLEM!')
        continue
      else:
        self.register_problem(problem)
        log.info('Problem saved to db')
      self.problems.put(problem)
      while self.problems.full():
        time.sleep(1)

    for i in range(self.NUM_SOLVERS):
      log.info('Putting None to finalize solvers')
      self.problems.put(None)

    while any(t.is_alive() for t in self.solvers):
      time.sleep(1)

    self.stop()

  def finished_problem(self, task_id):
    return bool(db.execute('SELECT task_id FROM task_state WHERE task_id=? AND task_state IN ("done", "expired") ',
                           [task_id]))


  def register_problem(self, task):
    db.execute('''INSERT INTO task_state (task_id, task_state)
                  VALUES(?, "todo");''', [task.id])
    db.commit()

  def problem_expired(self, task_id):
    db.execute('''INSERT OR REPLACE INTO task_state (task_id, task_state) VALUES (?, "expired") ''', [task_id])
    db.commit()

  def stop(self):
    log.info('Stopping solvers.')
    for s in self.solvers:
      s.stop()


class TrainSubmitter(SubmitterBase):
  NUM_TRAIN_PROBLEMS = 20

  def fetch_problems(self):
    for i in range(self.NUM_TRAIN_PROBLEMS):
      yield self.fetch_one_problem()

  def fetch_one_problem(self):
    train_data = self.client.train({'operators': OPERATORS, 'size': SIZE})

    challenge = train_data['challenge']
    task = Task(id=train_data['id'],
                operators=train_data['operators'],
                task_size=train_data['size'],
                data_points=[],
                program=challenge)

    sample_data = []
    fun = to_python(challenge)
    sample_args = generate_inputs()
    for arg in sample_args:
      sample_data.append((arg, fun(arg) % (1<<64)))

    task.data_points[:] = sample_data
    log.info('train problem: %s', challenge)
    return task


class RealTaskSubmitter(SubmitterBase):
  JSON_PATH = os.path.join(os.path.dirname(__file__), '..', 'problems.json')

  def tasks_json(self, size):
    tasks = json.load(open(self.JSON_PATH))
    return sorted([t for t in tasks if t['size'] == size and set(OPERATORS).issubset(
        set(t['operators']))], key=lambda t: (t['size'], t['id']), reverse=True)

  def fetch_problems(self):
    c = 0
    for task_json in self.tasks_json(SIZE):
      try:
        if self.finished_problem(task_json['id']):
          continue
        c += 1
        yield self.fetch_one_problem(task_json)
      except ProblemExpired:
        self.problem_expired(task_json['id'])
      except ProblemSolved:
        self.problem_expired(task_json['id'])
        task = Task(id=task_json['id'], task_size=0, program='', operators=[],
                    data_points=[])
        mark_task_done(task, '')
      if c == REAL_SOLVER_CUTOFF:
        break
      print 'real task %s of %s' % (c, REAL_SOLVER_CUTOFF)

  def fetch_one_problem(self, task_json):
    task = Task(id=task_json['id'],
                operators=frozenset(task_json['operators']),
                task_size=task_json['size'],
                data_points=[],
                program='')

    sample_args = generate_inputs()
    sample_values = self.get_program_values(task.id, sample_args)

    task.data_points[:] = zip(sample_args, sample_values)

    return task

  def get_program_values(self, task_id, args):
    payload = {
      'id': task_id,
      'arguments': map(lambda a: '0x%X' % a, args),
    }

    res = self.client.eval(payload)
    if res['status'] != 'ok':
      print 'error', res['message']
      return
    values = [int(server_value, 16) for server_value in res['outputs']]
    return values

class ManualSubmitter(RealTaskSubmitter):
  JSON_TASK = {u'program': u'''(lambda (x_17759) (fold x_17759 0 (lambda (x_17759
               x_17760) (if0 (xor (not (xor x_17759 x_17760)) 0) x_17759
                         x_17759))))''',
               u'operators': [u'tfold', 'if0', 'xor', 'not'],
               u'size': 12}

  def tasks_json(self, size):
    tasks = [self.JSON_TASK]
    return (t for t in tasks if t['size'] <= size)

  def fetch_problems(self):
    for task_json in self.tasks_json(SIZE):
      yield self.fetch_one_problem(task_json)

  def fetch_one_problem(self, task_json):
    task = Task(id='',
                operators=frozenset(task_json['operators']),
                task_size=task_json['size'],
                data_points=[],
                program=task_json['program'])

    sample_args = generate_inputs()

    sample_values = self.get_program_values(task_json['program'], sample_args)

    task.data_points[:] = zip(sample_args, sample_values)

    return task


  def get_program_values(self, program, args):
    payload = {
        'program': program,
        'arguments': map(lambda a: '0x%X' % a, args),
    }

    res = self.client.eval(payload)
    values = [int(server_value, 16) for server_value in res['outputs']]
    return values


class ManualSubmitterById(RealTaskSubmitter):
  JSON_TASK = {u'id': u'iVyGyToyUwzcfEbTuycDMlZW', u'operators': [u'not'], u'size': 3}

  def tasks_json(self, size):
    tasks = [self.JSON_TASK]
    return (t for t in tasks if t['size'] <= size)

  def fetch_problems(self):
    for task_json in self.tasks_json(3):
      yield self.fetch_one_problem(task_json)

  def fetch_one_problem(self, task_json):
    task = Task(id=task_json['id'],
                operators=frozenset(task_json['operators']),
                task_size=task_json['size'],
                data_points=[])

    sample_args = [0, 1, 2]
    for i in range(252):
      sample_args.append(random.randint(0, 1<<64))

    sample_values = self.get_program_values(task_json['id'], sample_args)

    task.data_points[:] = zip(sample_args, sample_values)

    return task


  def get_program_values(self, task_id, args):
    payload = {
        'id': task_id,
      'arguments': map(lambda a: '0x%X' % a, args),
    }

    res = self.client.eval(payload)
    if res['status'] != 'ok':
      print 'error', res['message']
      return
    values = [int(server_value, 16) for server_value in res['outputs']]
    return values


def main():
  #submitter = TrainSubmitter()
  #submitter = ManualSubmitter()
  submitter = RealTaskSubmitter()
  #submitter = ManualSubmitterById()
  try:
    submitter.start()
  except KeyboardInterrupt:
    log.info('keyboardinterrupt. stopping')
    submitter.stop()

if __name__ == '__main__':
  main()
